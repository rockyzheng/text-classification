#!/usr/bin/env python
# -*-coding=utf-8-*-

import numpy as np

class Word2vec(object):
    def __init__(self):
        self.wv = {}


    def load_w2v_array(self, path, vocab_processor, is_binary=False):
        """

        :param path:
        :param vocab_index2word: vocab index to word
        :param is_binary:
        :return:
        """

        if not is_binary:
            f = open(path, errors="ignore")
            m, n = f.readline().split()
            dim = int(n)

            print("%s words dim : %s"% (m, n ))
            for  i, line in enumerate(f):
                line = line.strip().split(" ")
                word = line[0]

                vec  =[float(v) for v in line[1:]]
                self.wv[word] = vec

        vocab_size = len(vocab_processor.vocabulary_)
        embedding = []

        bound =  np.sqrt(6.0) / np.sqrt(vocab_size)

        for i in range(vocab_size):
            word = vocab_processor.vocabulary_.reverse(i)
            if word in self.wv:
                embedding.append(self.wv.get(word))
            else:
                # todo 随机赋值为何?
                embedding.append(np.random.uniform(-bound, bound, dim));
        return np.array(embedding)



if __name__ == '__main__':
    w2v = Word2vec()
    w2v.load_word2vec("../data/jd_cv_cbow.txt")


