#!/usr/bin/env bash

python train.py --train_data_path ../data/boss_double_train.txt --embed_file    ../data/all_chat_vec.txt --num_classes 4
python test.py --test_data_path   ../data/boss_double_test.txt --checkpoint_dir ./runs/1510202188/checkpoints/ --num_classes 4



python train.py --train_data_path ../data/geek_double_train.txt --embed_file ../data/all_chat_vec.txt --num_classes 4
python test.py --test_data_path ../data/geek_double_test.txt --checkpoint_dir ./runs/1510202595/checkpoints/ --num_classes 4