#!/usr/bin/env python
# -*-coding=utf-8-*-

class TextCNNConfig(object):

    def __init__(self):
        self.learning_rate = 0.001
        self.batch_size = 128
        self.sequence_length = 28
        self.embedding_dim = 28
        self.num_hidden = 128
        self.num_classes = 2
        self.vocab_sizes = 0
        self.dropout_keep_prob = 0.5
        self.l2_lambda = 0.001
        self.optimizer = "Adam"

